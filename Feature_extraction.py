import numpy as np
import librosa
import librosa.display
#############Wavelet_Transform########
import pywt
import pywt.data
from pywt import wavedec

class FE(object):
        
        def __init__(self, sr, n_rows, n_col, dt, nMels, scales, cname, dname):
                #Librosa-TimeSeries
                self.sr = sr
                self.n_rows = n_rows
                self.n_col = n_col
                self.dt = dt
                self.nMels = nMels
                #PyWavelet
                self.scales = scales
                self.cname = cname
                self.dname = dname
                #Multilevel decomposition
                #self.mlevel = mlevel

        def CWT(self, y_tseries):
            coef, freqs = pywt.cwt(y_tseries, self.scales, self.cname, sampling_period=self.dt)            
            return coef, freqs

        def DWT(self, y_tseries, mlevel):
            cHP, cLP= pywt.dwt(y_tseries, wavelet = self.dname) #D: High pass Filter, A: Low pass filter
            #Multilevel decomposition using wavedec
            coeff = wavedec(y_tseries, self.dname, mlevel)                      
            
            return cHP, cLP, coeff
        
        def STFT(self, y_tseries, y_trim, windowS, pad_modeS):
            if self.sr < 10000:
                #D = np.abs(librosa.stft(y_trim, n_fft=self.n_rows, hop_length=self.n_col, window=sig.windows.hamming))
                fmin = 0.5 * self.sr * 2**(-6)
                D = np.abs(librosa.stft(y_trim, n_fft=self.n_rows, hop_length=self.n_col, window=windowS, pad_mode = pad_modeS))
                DB = librosa.amplitude_to_db(D, ref=np.max)
                mel = librosa.filters.mel(sr=self.sr, n_fft=self.n_rows, n_mels=self.nMels)
                S = np.abs(librosa.stft(y_tseries))                
                contrast = librosa.feature.spectral_contrast(S=S, sr=self.sr, fmin=fmin) #Falta flag para el Nyquist            
                S2 = librosa.feature.melspectrogram(y_trim, sr=self.sr, n_mels=self.nMels)
                f1 = librosa.feature.chroma_stft(y=y_tseries, sr=self.sr)#chroma_stft
                f7 = librosa.feature.mfcc(y=y_tseries, n_fft=self.n_rows, hop_length=self.n_col)
                f8 = librosa.power_to_db(S2, ref=np.max)
            else:
                #D = np.abs(librosa.stft(y_trim, n_fft=self.n_rows, hop_length=self.n_col, window=sig.windows.hamming))
                D = np.abs(librosa.stft(y_trim, n_fft=512, hop_length=2048, window=windowS))
                DB = librosa.amplitude_to_db(D, ref=np.max)
                mel = librosa.filters.mel(sr=self.sr, n_fft=self.n_rows, n_mels=self.nMels)
                S = np.abs(librosa.stft(y_tseries))            
                fmin = 0.5 * self.sr * 2**(-6)
                #contrast = librosa.feature.spectral_contrast(S=S, sr=self.sr, fmin=fmin) #Falta flag para el Nyquist            
                S2 = librosa.feature.melspectrogram(y_trim, sr=self.sr, n_mels=self.nMels)
                f1 = librosa.feature.chroma_stft(y=y_tseries, sr=self.sr)#chroma_stft
                f7 = librosa.feature.mfcc(y=y_tseries, n_fft=self.n_rows, hop_length=self.n_col)
                f8 = librosa.power_to_db(S2, ref=np.max)
            
            return DB, S, S2, f1, f7, f8
       
        
