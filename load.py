import librosa
import librosa.display
import numpy as np
import matplotlib.pyplot as plt
from IPython.display import Audio, display, Image, YouTubeVideo

class Loader(object):
      """ Load class is responsible for loagin the audio files"""
      def __init__(self, sample_rate, duration, mono, ruta):
          self.sample_rate = sample_rate
          self.duration = duration
          self.mono = mono      
          self.ruta = ruta
          ruta_root = ""
          self.ruta_audio = ruta_root+ruta 

      def load(self):
          signal = librosa.load(self.ruta_audio,
                                sr=self.sample_rate,
                                duration = self.duration,
                                mono=self.mono)[0] #only the time series signal from the tuple (y, sr)
      
          return signal

class MinMaxNormaliser:
          """MinMaxNormaliser applies min max normalisation to an array."""

          def __init__(self, min_val, max_val):
              self.min = min_val
              self.max = max_val

          def normalise(self, array):
              norm_array = (array - array.min()) / (array.max() - array.min())
              norm_array = norm_array * (self.max - self.min) + self.min
              return norm_array

          def denormalise(self, norm_array, original_min, original_max):
              array = (norm_array - self.min) / (self.max - self.min)
              array = array * (original_max - original_min) + original_min
              return array