#WebFramework Module
from flask import Flask, render_template, url_for, request, redirect
import pandas as pd
import numpy as np
###########Audio_Processing_Libraries##########
import os, sys
#pip install librosa
import librosa
import librosa.display
import numpy as np
import matplotlib.pyplot as plt
from IPython.display import Audio, display, Image, YouTubeVideo
##############AudioProcessing##########################
#from pydub import AudioSegment
#from pydub.utils import make_chunks
#############Wavelet_Transform########
import pywt
import pywt.data
###############Plot_Graphs########################
import plotly.express as px
import plotly.graph_objects
#############Plot_Embbedding##################
import json
import base64
from io import BytesIO



app = Flask(__name__)
sys.path.append("/")
from load import Loader
from Feature_extraction import FE

'''LIBROSA - AUDIO PROCESSING'''
sr = 100 ; t = 5; m = True #Fs=sr
audio = "Audios/Normal_Heart_Sound.wav" #r raw String
loader = Loader(sr, t, m, audio)
filename = loader.load()
aorticvalvesound,_= librosa.effects.trim(filename)
librosa.display.waveplot(filename, sr=sr)
N = filename.shape[0]; dt = 1/sr; time = np.arange(0, N) * dt
#plt.xlabel("Time"); plt.ylabel("Amplitude"); plt.show()

@app.route("/")
def index():    
    fig2=px.line(x=time, y=filename)
    fig2.update_xaxes(title_text='Time')
    fig2.update_yaxes(title_text='Amplitude')
    graphJSON = json.dumps(fig2, cls=plotly.utils.PlotlyJSONEncoder)
    return render_template('plt.html', graphJSON=graphJSON)
if __name__ == '__main__':
    app.run(port = 8000, debug=True)
